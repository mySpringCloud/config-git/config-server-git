package com.forezp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 * 启动config client之前，要先访问config server端的任意url，要不然config client启动失败，报错为某个值@Value注入失败，原因不明。
 * 
 */
@RestController
public class ConfigServerController {
	
	@RequestMapping(value = "/hi")
	public String hi(){
		return "Hi this is Config Server!";
	}
}
